import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math
import random

train_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_train_assignment2019.csv")
test_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_plotting_assignment2019.csv")

x_train = np.array(train_data['x'].as_matrix())
y_train = np.array(train_data['y'].as_matrix())
features_train = np.array(train_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())

x_test = np.array(test_data['x'].as_matrix())
features_test = np.array(test_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())

features_train = np.transpose(features_train)
features_test = np.transpose(features_test)

reg_factor = [ 10**-6, 10**-4, 10**-2, 10**-1] # reg_factor (this is same as lambda/alpha)
reg_factor2 = [ 10**0, 10**1, 10**2, 10**3 ]  # reg_factors for the evulation section of the assignment
reg_factors = [ 10**-6, 10**-4, 10**-2, 10**-1, 10**0, 10**1, 10**2, 10**3 ]

def create_data_matrix(features):
    X = np.array(features[0].transpose())
    for i in range(1,len(features[0])):
        X = np.column_stack((X, features_train[i]))
    return X

def ridge_regression(features_train, y_train, regularisation_factor):

    X = create_data_matrix(features_train)
    XX = X.transpose().dot(X)
    I = np.identity(len(XX))
    parameters = np.linalg.solve(XX + I*regularisation_factor, X.transpose().dot(y_train))
    return parameters

def eval_regression(parameters,features,y):
    rmse= 0
    X = np.array(features_train[0].transpose())
    for i in range(1,len(features_train[0])):
        X = np.column_stack((X, features_train[i]))
    E = X.dot(parameters)
    n = len(E)
    for i in range(0,n):
        E[i] = E[i] **2
    s = sum(E)
    rmse = math.sqrt(s/n)
    return rmse

def showRegression(features, w, x_test, iteration):
    #print(features)
    X = np.array(features[0].transpose())
    for i in range(1,len(features[:,0])):
        X = np.column_stack((X, features[i]))
    yhat = X.dot(w)
    colours = ['ro','bo','go','yo','co','mo','ko','ro']
    colours = ['r','b','g','y','c','m','k','r']
    plt.plot(x_test,yhat,colours[iteration])
    plt.ylim(top = 1000)
    plt.ylim(bottom = -1000)
    plt.xlim(left = -5)
    plt.xlim(right = 5)

def split_data(x):
    random.shuffle(x[:])
    print("x = ",x)
    return 0 



if 1==1:
    #plot the weighted function
    RMSE = []
    plt.clf()
    plt.plot(x_train,y_train,'bo')
    for i in range(0,len(reg_factors)):
        w = ridge_regression(features_train, y_train, reg_factors[i])
        showRegression(features_test, w, x_test, i)
        RMSE.append(eval_regression(w, features_train, y_train))
    plt.show()
    plt.clf()
    plt.plot(range(0,len(RMSE)),RMSE, 'b-')
    plt.ylabel("RMSE")
    plt.show()