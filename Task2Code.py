# reusable code from K means workshop to Assignment task 2
# 4 Input features for algorithm are:
#   stem_length, stem_width, leaf_length, leaf_width
#   300 rows of data

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from random import uniform
import math 

init_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task2 - dataset - plants.csv")

#print(init_data)

k = 3 # change this when needed (3/4 are required by the assignmnent)
data_rows = 300

#  set up the data read from the csv file to the program
data = np.zeros([4, 300])

data[0,:] = init_data["stem_length"].as_matrix()
data[1,:] = init_data["stem_diameter"].as_matrix()
data[2,:] = init_data["leaf_length"].as_matrix()
data[3,:] = init_data["leaf_width"].as_matrix()

# find the min and max values for each of the columns
data_ranges = np.zeros([4,2])
data_ranges[0,0] = min(data[0])
data_ranges[0,1] = max(data[0])
data_ranges[1,0] = min(data[1])
data_ranges[1,1] = max(data[1])
data_ranges[2,0] = min(data[2])
data_ranges[2,1] = max(data[2])
data_ranges[3,0] = min(data[3])
data_ranges[3,1] = max(data[3]) 
if 1 ==0:
    plt.clf()
    plt.plot(data[0],data[1], 'bo')
    plt.xlabel("Initial plot of data")
    plt.show()


def initialise_centroids(k): # use this in asignment as initialise_centroids()
    # k is the number of centroid points to generate
    centroids = np.empty([k,4]) # generate centroid array, one centroid for each column of data
    for x in range (0,k):
        np.random.seed(i)
        centroids[x,0] = uniform(data_ranges[0,0],data_ranges[0,1]) # create stem_length val in range
        centroids[x,1] = uniform(data_ranges[1,0],data_ranges[1,1]) # create stem_width val in range
        centroids[x,2] = uniform(data_ranges[2,0],data_ranges[2,1]) # create stem_length val in range
        centroids[x,3] = uniform(data_ranges[3,0],data_ranges[3,1]) # create stem_width val in range
        
        #print("Generate Centroid %s = (%s, %s, %s, %s)" %(x, centroids[x,0], centroids[x,1], centroids[x,2], centroids[x,3]))
    return centroids

centroids = initialise_centroids(k)

def compute_euclidean_distance(vec0, vec1): # now input vectors can be of any dimsion and this still should work.
    #print("\n\n\nCompute Euclidean Distance")
    sides = np.zeros([vec0.size]) # array of sides for number of dimensions
    sum = 0
    for i in range(0,sides.size-1):
        #print("\ni = ", i, "\nvec0 = ", vec0, "\nvec1 = ",vec1)
        sides[i] = (vec0[i] - vec1[i])**2
        sum = sum + sides[i]
    distance = math.sqrt(sum)
    #print("computing distance")
    return distance




def kmeans(data, k):
    # initalise variables
    iteration_dist = []
    
    #print("\nk = ", k)
    prev_nearest_centroid = np.zeros(data[0].size)
    cond = True
    iteration = 0
    
    while(cond): # until not change between clusters -- change condition
        dist_sum = 0
        
        # start of what was find_shortest_path()
        size = data[0].size # number of rows in the data
        nearest_centroid = np.empty(size) # which centroid is the nearest to each of the data points
       # c_vec = np.zeros([k,2]) # initialise 
        distances = np.zeros([k]) # one distance for each centroid 
        for i in range(0,size): # for all elements in the dataset
             
            #d_vec = np.array([data[0,i],data[1,i]]) # --> may need top change this is 4-D once everything else is working and figure out what to do with all the data
            for x in range(0,k): # vector for each centroid
                distances[x] = compute_euclidean_distance(data[:,i],centroids[x]) # distances
            dist_sum = dist_sum + min(distances)
            tmp = 0
            for x in range(1,k): # finds which distances is the shortest without already knowing how many possible distances there are to check
                if distances[tmp] > distances[x]:
                    tmp = x
                    #print("tmp - ", tmp)
                nearest_centroid[i] = tmp # assigned which centoid is nearest to the datapoint

        
        #print(nearest_centroid)
        if 1 ==0:
            plt.clf()
            for i in range(0,data[0].size):
                if nearest_centroid[i] == 0:
                    plt.plot(data[0,i],data[1,i],'go' )
                elif nearest_centroid[i] == 1.:
                    plt.plot(data[0,i],data[1,i],'ro' )
                elif nearest_centroid[i] == 2.:
                    plt.plot(data[0,i],data[1,i],'bo' )
                elif nearest_centroid[i] == 3.:
                    plt.plot(data[0,i],data[1,i], 'yo')
            plt.xlabel("Plot of data in clusters")
           # plt.show()
        
        iteration_dist.append(dist_sum) # save the sum of distances from data points to the closest centroid for the second graph
        
        # find mean co-ords of current clusters to make new centroids
        cluster_sums = np.zeros([k, 5])
        for i in range(0,data[0].size):
            for x in range(0,k): # one iteration for each centroid as could be either 3 or 4 
                if nearest_centroid[i] == x:
                    cluster_sums[x, 0] = cluster_sums[x, 0] + data[0,i] # sums of all 4 dimensons
                    cluster_sums[x, 1] = cluster_sums[x, 1] + data[1,i]
                    cluster_sums[x, 2] = cluster_sums[x, 2] + data[2,i]
                    cluster_sums[x, 3] = cluster_sums[x, 3] + data[3,i]
                    cluster_sums[x, 4] +=1
                    
        #assign new centroids
        for i in range(0,k):
            centroids[i,0] = cluster_sums[i, 0]/cluster_sums[i, 4]
            centroids[i,1] = cluster_sums[i, 1]/cluster_sums[i, 4]
            centroids[i,2] = cluster_sums[i, 2]/cluster_sums[i, 4]
            centroids[i,3] = cluster_sums[i, 3]/cluster_sums[i, 4]
           
        # check if any centroid has been removed & replace it
        for i in range(0,k):
            for x in range(0,4):
                if math.isnan(centroids[i,x]):
                    #print(True)
                    centroids[i,x] = uniform(data_ranges[x,0],data_ranges[x,1])

        # kill the loop if goal condition is met
        if (nearest_centroid == prev_nearest_centroid).all():
            cond = False
            print("\n\nIteration = ", iteration, "  -  break due nearest_centroid array match")
        prev_nearest_centroid[:] = nearest_centroid[:]
        iteration +=1
    
    #print(data)
    #print(nearest_centroid)
    if 1 == 0: # hide the plot 
        plt.clf()
        for i in range(0,data[0].size):
            if nearest_centroid[i] == 0:
                plt.plot(data[0,i],data[1,i],'go' )
            elif nearest_centroid[i] == 1.:
                plt.plot(data[0,i],data[1,i],'ro' )
            elif nearest_centroid[i] == 2.:
                plt.plot(data[0,i],data[1,i],'bo' )
            elif nearest_centroid[i] == 3.:
                plt.plot(data[0,i],data[1,i], 'yo')
        plt.xlabel("Plot of data in clusters")
        plt.show()
            
        plt.clf()
        plt.plot(range(0,iteration),iteration_dist, 'o-')
        plt.xlabel("Iteration")
        plt.ylabel("SSE")
        plt.show()
    
    #print(iteration_dist)
    return centroids, nearest_centroid, iteration_dist

print("\n\n")
# objective function = average squared Euclidean distance  
# kmeans call for algorithm
list_nearest = []
list_dist = []
smallest = 0
tmp = 10000
for i in range(0,5):
    centroids = initialise_centroids(k)
    cents, nearest_cents, dist = kmeans(data, k)
    list_nearest.append(nearest_cents)
    list_dist.append(dist)
    if tmp > dist[len(dist)-1]:
        smallest = i


plt.clf()
for i in range(0,data[0].size):
    if list_nearest[smallest][i] == 0:
        plt.plot(data[0,i],data[1,i],'go' )
    elif list_nearest[smallest][i] == 1.:
        plt.plot(data[0,i],data[1,i],'ro' )
    elif list_nearest[smallest][i] == 2.:
        plt.plot(data[0,i],data[1,i],'bo' )
    elif list_nearest[smallest][i] == 3.:
        plt.plot(data[0,i],data[1,i], 'yo')
plt.xlabel("Plot of data in clusters")
plt.show()
    
plt.clf()
plt.plot(range(0,len(list_dist[smallest])),list_dist[smallest], 'o-')
plt.xlabel("Iteration")
plt.ylabel("SSE")
plt.show()