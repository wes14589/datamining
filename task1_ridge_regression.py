import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

train_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_train_assignment2019.csv")
test_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_plotting_assignment2019.csv")
x_train = np.array(train_data['x'].as_matrix())
y_train = np.array(train_data['y'].as_matrix())
features_train = np.array(train_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())
#print(np.transpose(features))
features_train = np.transpose(features_train)
x_test = np.array(test_data['x'].as_matrix())
#y_test = np.array(test_data['y'].as_matrix())

plt.clf()
plt.plot(x_train,y_train,'bo')
plt.xlabel("Plot of initial training data points")
plt.show()


def getPolynomialDataMatrix(x, degree): # set up martix for poly_reg
    X = np.ones(x.shape)
    for i in range(1,degree + 1):
        X = np.column_stack((X, x ** i))
    return X
    
print(getPolynomialDataMatrix(x_train, 4))


def getWeightsForPolynomialFit(x,y,degree):
    X = getPolynomialDataMatrix(x, degree)

    XX = X.transpose().dot(X)
    w = np.linalg.solve(XX, X.transpose().dot(y_train))

    return w



plt.figure()
plt.plot(x_train,y_train, 'bo')


def getPoly(x_train,y_train,x_test, d):
    w = getWeightsForPolynomialFit(x_train,y_train,d)
    Xtest = getPolynomialDataMatrix(x_test, d)
    ytest = Xtest.dot(w)
    plt.plot(x_train,y_train,'bo')
    plt.plot(x_test, ytest, 'r')
    plt.xlabel(d)
    plt.show()


for i in range(1,4):
    getPoly(x_train, y_train, x_test, i)





def ridge_regression(features_train, y_train, regularisationFactor): # 
    # my code
    
    
    parameters = 0
    return parameters # a weight for each feature