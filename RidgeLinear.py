import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import math

train_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_train_assignment2019.csv")
test_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_plotting_assignment2019.csv")
x_train = np.array(train_data['x'].as_matrix())
y_train = np.array(train_data['y'].as_matrix())
features_train = np.array(train_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())
#print(np.transpose(features))
features_train = np.transpose(features_train)
x_test = np.array(test_data['x'].as_matrix())
#y_test = np.array(test_data['y'].as_matrix())
features_test = np.array(test_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())

reg_factor = [ 10**-6, 10**-4, 10**-2, 10**-1] # reg_factor (this is same as lambda/alpha)
reg_factor2 = [ 10**0, 10**1, 10**2, 10**3 ]  # reg_factors for the evulation section of the assignment
error_list = np.zeros([15])
if 1 == 0:
    plt.clf()
    plt.plot(x_train,y_train,'bo')
    plt.xlabel("Plot of initial training data points")
    plt.show()



def getRidgeMatrix(x):
    X = np.ones(x.shape)
    X = np.column_stack((X, x))
    return X



def getWeightsForRidge(x, y, lam):
    X = getRidgeMatrix(x) # gets the array for the X data

    XX = X.transpose().dot(X) # X(transpose)X  --> **-1
    
    I = np.identity(len(XX))
    # w = (XtX + lambda*I)^-1 *Xty
    w = np.linalg.solve(XX + lam*I, X.transpose().dot(y_train))
    
    error = X.dot(w)
    error_list[0] = error.dot(error)
    print("Weights = ", w, "\nSSE = ", error_list[0])
    return w




def getRidge(x_train, y_train, x_test):
    w = getWeightsForRidge(x_train, y_train, reg_factor[3])
    Xtest = getRidgeMatrix(x_test)
    ytest = Xtest.dot(w)
    plt.plot(x_train, y_train, 'yo')
    plt.plot(x_test, ytest, 'g')
    plt.ylim(top=400)
    plt.ylim(bottom=-400)
    
    plt.xlabel("Ridge")
    plt.show()
    
    # return the weightings of the function ???



getRidge(x_train, y_train, x_test) # linear

print("Error list - ", error_list)


#getRidge(x_train, y_train, x_test, 11)




print("Test ridge with features")
getRidge(features_train[0], y_train,features_test[0], 3)






RMSEtrain = np.zeros((11,1))
RMSEtest = np.zeros((11,1))


for i in range(1,12): # x^1 to x^12
    Xtrain = getRidgeMatrix(x_train, i)
    Xtest = getRidgeMatrix(x_test, i)
    
    w = getWeightsForRidge(x_train, y_train, i, 0) # lam = 0 for this example
    
    RMSEtrain[i-1] = math.sqrt(np.mean((Xtrain.dot(w) - y_train)**2))
    #MSEtest[i-1] = np.mean((Xtest.dot(w) - y_test)**2)
    #print("\n\n")
print(RMSEtrain)


plt.figure();
plt.semilogy(range(1,12), RMSEtrain)
#plt.semilogy(range(1,12), MSEtest)
plt.legend(('SSE on training set', 'SSE on test set'))
plt.savefig('polynomial_evaluation.png')
plt.show()



def ridge_regression(features_train, y_train, regularisationFactor): # replace x with a one column of the features
    # my code
    parameters = []
    lam = reg_factor[0] # --> change this when running tests for the report
    for i in range(0,12): # len(features_train[0])
        parameters[i] = getWeightsForRidge(features_train[i], y_train, 1, lam)
    
    return parameters # a weight for each feature


def eval_regression(parameters, features, y):
    rmse = 0
    
    return rmse

