# -*- coding: utf-8 -*-
"""
Created on Mon Mar 11 17:29:04 2019

@author: tomwe
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

train_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_train_assignment2019.csv")
test_data = pd.DataFrame.from_csv("CMP3744M_ADM_Assignment 1_Task1 - dataset - ridge_regression/regression_plotting_assignment2019.csv")
x_train = np.array(train_data['x'].as_matrix())
y_train = np.array(train_data['y'].as_matrix())
features_train = np.array(train_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())
#print(np.transpose(features))
features_train = np.transpose(features_train)
x_test = np.array(test_data['x'].as_matrix())
features_test = np.array(test_data[['features0','features1','features2','features3','features4','features5','features6','features7','features8','features9','features10','features11']].as_matrix())


#print(features_train, "\n-\n")
if 1 == 0:
    plt.clf()
    plt.plot(x_train,y_train,'bo')
    plt.xlabel("Plot of initial training data points")
    plt.show()


if 1 == 0:
    for i in range(0,12):
        plt.clf()
        plt.plot(features_train[i],y_train,'bo')
        plt.xlabel("Features {0} by y_train".format(i))
        plt.show()



def getMultipleDataMatrix(x, degree): # set up martix for poly_reg
    X = np.array(x[0].transpose())
    for i in range(1,degree):
        X = np.column_stack((X, x[i]))
    #print("X ---.",X)

    
    # [1 , f_t[1,1], f_t[1,2]..., f_t[1,d]]      n = number of data points
    # [1 , f_t[2,1], f_t[2,2]..., f_t[2,d]]      d = number of features
    # [1 , f_t[n,1], f_t[n,2]..., f_t[n,d]]    f_t = features_train
    return X



def getWeightsForMultipleFit(x,y,degree): # where degree is the number of input values
    X = getMultipleDataMatrix(x, degree)
    #print(X)
    #for i in range(0,degree):
        
    XX = X.transpose().dot(X)
    #print("X,transpose() - ",X.transpose())
    w = np.linalg.solve(XX, X.transpose().dot(y_train))

    #print("This might be the right value for w??   -  " ,w )
    return w


def getMulti(features_train,y_train,x_test):
    d = 12 # len(feature_train) mone for each feature
    w = getWeightsForMultipleFit(features_train,y_train,12)
    print("Weights = ",w)
    
    
    
    
    
    plt.clf()
    plt.plot(range(0,len(w)),w, 'o-')
    plt.xlabel("Feature")
    plt.ylabel("Weight factor")
    plt.show()
    
    
    Xtest = getMultipleDataMatrix(x_test, d)
    ytest = Xtest.dot(w)
    
    if 1 == 0:
        plt.plot(x_train,y_train,'bo')
        plt.plot(x_test, ytest, 'r')
        plt.xlabel(d)
        plt.show()



print(getMulti(features_train, y_train, features_test))

"""



def ridge_regression(features_train, y_train, regularisationFactor): # 
    # my code
    
    
    parameters = 0
    return parameters # a weight for each feature
"""
